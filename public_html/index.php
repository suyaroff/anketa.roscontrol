<? include('send.php'); ?><!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Some</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
<div class="wrapper">
    <header>
        <div class="header-left">
            <div class="logo-box">
                <img src="img/logo2.png" class="logo-2" alt="">
                <img src="img/logo3.png" alt="">
            </div>
            <div class="logo-text">
                Федеральная программа сертификации <span class="red">*</span>.<br>
                На основе СДС «Россертификация».
            </div>
        </div>
        <div class="header-center">
            <img src="img/logo.png" alt="Росконтроль">
            <h1>РОСКОНТРОЛЬ</h1>
            <span class="sub-desc">единый центр сертификации</span>
        </div>
        <div class="header-right">
            Приведение деятельности организации в соответствие<br>
            с требованями № 152-ФЗ «О персональных данных».
        </div>
    </header>
    <main>
        <h2>Форма заявки</h2>
        <form action="" method="post" class="main-form">
            <div class="form-item row-first">
                <div class="form-label">Выберите услугу</div>
                <div class="radio">
                    <input type="radio" id="radio_1" name="service" value="Бесплатная консультация эксперта">
                    <label for="radio_1">Бесплатная консультация эксперта</label>
                </div>
                <div class="radio">
                    <input type="radio" id="radio_2" name="service" value="Расчет стоимости документации" checked>
                    <label for="radio_2">Расчет стоимости документации</label>
                </div>
            </div>
            <div class="form-row">
                <div class="form-item">
                    <div class="form-label" style="font-size: 16px;">ИНН организации</div>
                    <input type="text" name="inn" value="<? echo $inn; ?>" class="form-input" required>
                </div>
                <div class="form-item">
                    <div class="form-label">Количество работников</div>
                    <input type="text" name="count" class="form-input">
                </div>
            </div>

            <div class="form-row">
                <div class="form-item">
                    <div class="form-label">Контактный номер телефона</div>
                    <div class="form-sub-label">Куда вам звонить</div>
                    <input type="text" name="phone" class="form-input">
                </div>
                <div class="form-item">
                    <div class="form-label">Адрес электронной почты</div>
                    <div class="form-sub-label">Для напраления информации</div>
                    <input type="text" name="email" value="<? echo $email; ?>" class="form-input">
                </div>
            </div>

            <div class="form-item">
                <div class="radio">
                    <input type="checkbox" name="agree" value="1" id="radio_3" checked required>
                    <label for="radio_3">Я принимаю условия передачи информации в соответствии с политикой конфиденциальности</label>
                </div>
            </div>
            <p class="row-button">
                <button type="submit" class="button">Отправить заявку</button>
            </p>
        </form>
    </main>
</div>

<div class="over-layer <? if (!$is_send) echo 'hide'; ?>">
    <div class="message">
        <div class="message-title">Ваша заявка успешно отправлена!</div>
        <div class="message-text">
            Эксперт СДС "Россертификация" свяжется с вами в порятдке очереди.<br>
            <div class="redirect-message">Через <span id="timer">3</span> секунды вы будите перенаправлены на <a href="<? echo $main_site;?>">основной сайт</a></div>
        </div>
    </div>
</div>

<? if ($is_send) { ?>
    <script>
        var tmr = document.getElementById('timer');
        var sc = 3;
        setInterval(function () {
            sc--;
            tmr.innerHTML = sc;
        }, 1000);
        setTimeout(function () {
            window.location = '<? echo $main_site;?>';
        }, 3000);
    </script>
<? } ?>

</body>
</html>